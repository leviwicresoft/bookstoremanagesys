package bookstoreMS.user.panel;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import bookstoreMS.user.business.factory.UserEbiFactory;
import bookstoreMS.user.dao.UserTypeEnum;
import bookstoreMS.user.vo.UserModel;

public class UpdatePanel extends JFrame{
    public UpdatePanel(JFrame frame,int uuid){
    	JFrame jframe = frame;
        jframe.setSize(600,800);
        jframe.setLayout(null);
        JPanel jpanel = new JPanel();
        jpanel.setLayout(null);
        jpanel.setSize(600,800);
        Font font = new Font("SERIF", Font.BOLD, 20);
        JLabel addlabel = new JLabel();
        addlabel.setFont(font);
        addlabel.setText("Add User");
        addlabel.setBounds(250,25,100,50);
        JLabel uuidlabel = new JLabel("Uuid");
        uuidlabel.setBounds(100,100,100,50);
        JTextField uuidtext = new JTextField();
        UserModel user = UserEbiFactory.getUserEbo().getSingle(uuid);
        uuidtext.setText(String.valueOf(user.getUuid()));
        uuidtext.setBounds(200,100,100,50);
        uuidtext.setEditable(false);
        
        JLabel namelabel = new JLabel("Name");
        namelabel.setBounds(100,200,100,50);
        JTextField nametext = new JTextField();
        nametext.setBounds(200,200,100,50);
        nametext.setText(user.getName());
        
        JLabel typelabel = new JLabel("User Type");
        typelabel.setBounds(100,300,100,50);
        JComboBox<String> typeoption = new JComboBox<String>();
        for(UserTypeEnum usertype : UserTypeEnum.values()){
        	typeoption.addItem(usertype.getName());
         }
        typeoption.setSelectedItem(UserTypeEnum.getNameByType(user.getType()));
        typeoption.setBounds(200,300,100,50);
        
        JLabel passwordlabel = new JLabel("Password");
        passwordlabel.setBounds(100,400,100,50);
        JPasswordField passwordtext = new JPasswordField();
        passwordtext.setBounds(200,400,100,50);
        passwordtext.setText(user.getPwd());
        
        JLabel confirmlabel = new JLabel("Confirm Password");
        confirmlabel.setBounds(320,400,120,50);
        JPasswordField confirmtext = new JPasswordField();
        confirmtext.setBounds(430,400,100,50);
        confirmtext.setText(user.getPwd());
        JButton submit = new JButton("Update");
        submit.setBounds(100,550,100,50);
        submit.addActionListener(new ActionListener(){
        	public void actionPerformed(ActionEvent e){
        		int uuid = Integer.parseInt(uuidtext.getText());
        		String name = nametext.getText();
        		String typename = typeoption.getSelectedItem().toString();
        		int type = UserTypeEnum.getTypeByName(typename);
        		String password = new String(passwordtext.getPassword());
        		String confirmpassword = new String(confirmtext.getPassword());
        		if (!password.equals(confirmpassword)|| password.length()<6){
        			JOptionPane.showConfirmDialog(null, "Password must be the same and more than 6 characters");
        		}else{
        		UserModel user = new UserModel();
        		user.setUuid(uuid);
        		user.setName(name);
        		user.setType(type);
        		user.setPwd(password);
        		if(UserEbiFactory.getUserEbo().update(user)){
        			
        			jframe.getContentPane().removeAll();
    				jframe.getContentPane().add(new ListPanel(jframe));
    				jframe.getContentPane().validate();
    				jframe.getContentPane().repaint();
    								
        		}
        		}
        	}
        });
        JButton cancel = new JButton("Return");
        cancel.setBounds(400,550,100,50);
        cancel.addActionListener(new ActionListener(){
        	public void actionPerformed(ActionEvent e){
        		jframe.getContentPane().removeAll();
    			jframe.getContentPane().add(new ListPanel(jframe));
    			jframe.getContentPane().validate();
    			jframe.getContentPane().repaint();
    							
        	}
        });
        
        jpanel.add(addlabel);
        jpanel.add(uuidlabel);
        jpanel.add(uuidtext);
        jpanel.add(namelabel);
        jpanel.add(nametext);
        jpanel.add(typelabel);
        jpanel.add(typeoption);
        jpanel.add(passwordlabel);
        jpanel.add(passwordtext);
        jpanel.add(confirmlabel);
        jpanel.add(confirmtext);
        jpanel.add(submit);
        jpanel.add(cancel);
        jframe.add(jpanel);
        
        jframe.setVisible(true);
    }
}
