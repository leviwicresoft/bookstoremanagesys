package bookstoreMS.user.panel;

import java.awt.Font;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.awt.Window;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListModel;

import bookstoreMS.user.business.factory.UserEbiFactory;
import bookstoreMS.user.dao.factory.UserDaoFactory;
import bookstoreMS.user.vo.UserModel;

public class ListPanel extends JFrame {
	    public ListPanel(JFrame frame){
		JFrame jframe = frame;
	
		jframe.setSize(600,600);
		
		jframe.setLocation(100,100);
		jframe.setLayout(null);
		JPanel jpanel = new JPanel();
		jpanel.setLayout(null);
		jpanel.setSize(600,600);
		
	
		
		JButton button = new JButton("ADD Users");
		button.setFocusPainted(false);
		button.setBounds(50,320,100,50);
		button.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				if(e.getSource() == button){
					jframe.getContentPane().removeAll();
					jframe.getContentPane().add(new AddPanel(jframe));
					jframe.getContentPane().validate();
					jframe.getContentPane().repaint();
									
					
				}
			}
		});
		JLabel label = new JLabel();
		label.setBounds(new Rectangle(250,100,100,25));
		
		Font font = new Font("Serif", Font.BOLD, 20);
		label.setFont(font);
		label.setText("User List");
		
		@SuppressWarnings("unchecked")
		ArrayList<UserModel> list = new ArrayList();
		list = UserDaoFactory.getUserDao().getAll();
		Object obj[] = list.toArray();
		@SuppressWarnings("unchecked")
		JList jlist = new JList(obj);
		
		JScrollPane jspane = new JScrollPane(jlist);
		jspane.setBounds(200, 150, 150, 150);
		
		JButton updatebutton = new JButton("Update User");
		updatebutton.setFocusPainted(false);
		updatebutton.setBounds(150,320,100,50);
		updatebutton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				UserModel user = (UserModel)jlist.getSelectedValue();
				
				if(user != null){
				int uuid = user.getUuid();
				jframe.getContentPane().removeAll();
				jframe.getContentPane().add(new UpdatePanel(jframe,uuid));
				jframe.getContentPane().validate();
				jframe.getContentPane().repaint();
			    }else{
				    JOptionPane.showConfirmDialog(null, "please select an item");
			     }
			}
		});
		
		JButton deletebutton = new JButton("Delete");
		deletebutton.setBounds(250,320,100,50);
		deletebutton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				UserModel user = (UserModel)jlist.getSelectedValue();
				if(user != null ){
					if(JOptionPane.showConfirmDialog(null, "are you sure to delete")==JOptionPane.YES_OPTION){
					UserEbiFactory.getUserEbo().delete(user.getUuid());
					jframe.getContentPane().removeAll();
					jframe.getContentPane().add(new ListPanel(jframe));
					jframe.getContentPane().validate();
					jframe.getContentPane().repaint();
					}
				}else{
					JOptionPane.showMessageDialog(null,  "Please select an item");
					}
			}
		});
		JButton Querybutton = new JButton("Query");
		Querybutton.setBounds(350,320,100,50);
		Querybutton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				
				    
				
					jframe.getContentPane().removeAll();
					jframe.getContentPane().add(new QueryPanel(jframe));
					jframe.getContentPane().validate();
					jframe.getContentPane().repaint();
					}
				
			
		});
		jpanel.add(Querybutton);
		jpanel.add(deletebutton);
		jpanel.add(updatebutton);
		jpanel.add(label);
		jpanel.add(button);
		jpanel.add(jspane);
		jframe.add(jpanel);
		
		jframe.setVisible(true);
		}
	    
     public ListPanel(JFrame frame, List list){
    	
    	 JFrame jframe = frame;
    		
 		jframe.setSize(600,600);
 		
 		jframe.setLocation(100,100);
 		jframe.setLayout(null);
 		JPanel jpanel = new JPanel();
 		jpanel.setLayout(null);
 		jpanel.setSize(600,600);
 		
 	
 		
 		JButton button = new JButton("ADD Users");
 		button.setFocusPainted(false);
 		button.setBounds(50,320,100,50);
 		button.addActionListener(new ActionListener(){
 			public void actionPerformed(ActionEvent e){
 				if(e.getSource() == button){
 					jframe.getContentPane().removeAll();
 					jframe.getContentPane().add(new AddPanel(jframe));
 					jframe.getContentPane().validate();
 					jframe.getContentPane().repaint();
 									
 					
 				}
 			}
 		});
 		JLabel label = new JLabel();
 		label.setBounds(new Rectangle(250,100,100,25));
 		
 		Font font = new Font("Serif", Font.BOLD, 20);
 		label.setFont(font);
 		label.setText("User List");
 		
 		Object obj[] = list.toArray();
 		
 		@SuppressWarnings("unchecked")
		JList jlist = new JList(obj);
 		
 		JScrollPane jspane = new JScrollPane(jlist);
 		jspane.setBounds(200, 150, 150, 150);
 		
 		JButton updatebutton = new JButton("Update User");
 		updatebutton.setFocusPainted(false);
 		updatebutton.setBounds(150,320,100,50);
 		updatebutton.addActionListener(new ActionListener(){
 			public void actionPerformed(ActionEvent e){
 				UserModel user = (UserModel)jlist.getSelectedValue();
 				
 				if(user != null){
 				int uuid = user.getUuid();
 				jframe.getContentPane().removeAll();
 				jframe.getContentPane().add(new UpdatePanel(jframe,uuid));
 				jframe.getContentPane().validate();
 				jframe.getContentPane().repaint();
 			    }else{
 				    JOptionPane.showConfirmDialog(null, "please select an item");
 			     }
 			}
 		});
 		
 		JButton deletebutton = new JButton("Delete");
 		deletebutton.setBounds(250,320,100,50);
 		deletebutton.addActionListener(new ActionListener(){
 			public void actionPerformed(ActionEvent e){
 				UserModel user = (UserModel)jlist.getSelectedValue();
 				if(user != null ){
 					if(JOptionPane.showConfirmDialog(null, "are you sure to delete")==JOptionPane.YES_OPTION){
 					UserEbiFactory.getUserEbo().delete(user.getUuid());
 					
 					jframe.getContentPane().removeAll();
 					jframe.getContentPane().add(new ListPanel(jframe));
 					jframe.getContentPane().validate();
 					jframe.getContentPane().repaint();
 					}
 				}else{
 					JOptionPane.showMessageDialog(null,  "Please select an item");
 					}
 			}
 		});
 		JButton Querybutton = new JButton("Query");
 		Querybutton.setBounds(350,320,100,50);
 		Querybutton.addActionListener(new ActionListener(){
 			public void actionPerformed(ActionEvent e){
 				
 				    
 				
 					jframe.getContentPane().removeAll();
 					jframe.getContentPane().add(new QueryPanel(jframe));
 					jframe.getContentPane().validate();
 					jframe.getContentPane().repaint();
 					}
 				
 			
 		});
 		jpanel.add(Querybutton);
 		jpanel.add(deletebutton);
 		jpanel.add(updatebutton);
 		jpanel.add(label);
 		jpanel.add(button);
 		jpanel.add(jspane);
 		jframe.add(jpanel);
 		
 		jframe.setVisible(true);
	 
}

}
