package bookstoreMS.user.panel;

import java.awt.Font;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import bookstoreMS.user.business.factory.UserEbiFactory;
import bookstoreMS.user.dao.UserTypeEnum;
import bookstoreMS.user.vo.QueryModel;
import bookstoreMS.user.vo.UserModel;

public class QueryPanel extends JFrame{
	public QueryPanel(JFrame frame){
		JFrame jframe = frame;
	    jframe.setSize(600,800);
	    jframe.setLayout(null);
	    JPanel jpanel = new JPanel();
	    jpanel.setLayout(null);
	    jpanel.setSize(600,800);
	    Font font = new Font("SERIF", Font.BOLD, 20);
	    JLabel addlabel = new JLabel();
	    addlabel.setFont(font);
	    addlabel.setText("Search Result");
	    addlabel.setBounds(250,25,100,50);
	    JLabel uuidlabel = new JLabel("Uuid");
	    uuidlabel.setBounds(100,100,100,50);
	    JTextField uuidtext = new JTextField();
	    uuidtext.setBounds(200,100,100,50);
	    
	    JLabel namelabel = new JLabel("Name");
	    namelabel.setBounds(100,200,100,50);
	    JTextField nametext = new JTextField();
	    nametext.setBounds(200,200,100,50);
	    
	    JLabel typelabel = new JLabel("User Type");
	    typelabel.setBounds(100,300,100,50);
	    JComboBox<String> typeoption = new JComboBox<String>();
	    typeoption.addItem("Please select");
	    for(UserTypeEnum usertype : UserTypeEnum.values()){
	    	typeoption.addItem(usertype.getName());
	    }                                                        
	    
	    typeoption.setBounds(200,300,150,50);
	    
	   
	    
	    JButton search = new JButton("Search");
	    search.setBounds(100,500,100,50);
	    search.addActionListener(new ActionListener(){
	    	public void actionPerformed(ActionEvent e){
	    		int uuid =0;
	    		try{
	    		uuid = Integer.parseInt(uuidtext.getText().trim());
	    		}catch(Exception exception ){
	    			uuid = 0;
	    		}
	    		String name;
	    		try{
	    		name = nametext.getText();
	    		}catch (Exception exception){
	    			name = "";
	    		}
	    		String typename = typeoption.getSelectedItem().toString();
	    		int typeid=0;
	    		for(UserTypeEnum type : UserTypeEnum.values())
	    		 if(typename.equals(type.getName())){
	    			 typeid = type.getType();
	    		 }
	    		
	    		QueryModel qm = new QueryModel();
	    		qm.setUuid(uuid);
	    		qm.setName(name);
	    		qm.setType(typeid);
	    		List userlist;
	    		  userlist = UserEbiFactory.getUserEbo().getByCondition(qm);
	    		  
	    		if((uuid == 0 && name.equals("")) && typeid ==0){
	    			if(JOptionPane.showConfirmDialog(null, "please select")<=1){
	    				jframe.getContentPane().removeAll();
						jframe.getContentPane().add(new QueryPanel(jframe));
						jframe.getContentPane().validate();
						jframe.getContentPane().repaint();
										
	    			}else{
	    			userlist = new ArrayList();
	    		}
	    		}
	    	    
	    		 	    	
	    	    	jframe.getContentPane().removeAll();
					jframe.getContentPane().add(new ListPanel(jframe, userlist));
					jframe.getContentPane().validate();
					jframe.getContentPane().repaint();
									
	    		
	    		}
	    	}
	    );
	    JButton cancel = new JButton("Return");
	    cancel.setBounds(400,500,100,50);
	    cancel.addActionListener(new ActionListener(){
	    	public void actionPerformed(ActionEvent e){
	    		jframe.getContentPane().removeAll();
				jframe.getContentPane().add(new ListPanel(jframe));
				jframe.getContentPane().validate();
				jframe.getContentPane().repaint();
								
	    	}
	    });
	    
	    jpanel.add(addlabel);
	    jpanel.add(uuidlabel);
	    jpanel.add(uuidtext);
	    jpanel.add(namelabel);
	    jpanel.add(nametext);
	    jpanel.add(typelabel);
	    jpanel.add(typeoption);
	   
	    jpanel.add(search);
	    jpanel.add(cancel);
	    jframe.add(jpanel);
	    
	    jframe.setVisible(true);
		}  
	}

