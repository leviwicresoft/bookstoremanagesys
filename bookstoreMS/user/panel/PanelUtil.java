package bookstoreMS.user.panel;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class PanelUtil {

	public PanelUtil(JFrame jframe, JFrame frame ){

		jframe.getContentPane().removeAll();
		jframe.getContentPane().add(frame);
		jframe.getContentPane().validate();
		jframe.getContentPane().repaint();
						
	}
}
