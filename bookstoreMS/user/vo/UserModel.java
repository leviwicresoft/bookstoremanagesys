package bookstoreMS.user.vo;

import java.io.Serializable;

import bookstoreMS.user.dao.UserTypeEnum;

public class UserModel implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int  uuid;
	private String name;
	private int type;
	private String pwd;
	
	public void setUuid(int uuid2){
		this.uuid = uuid2;
	}
	public int getUuid(){
		return this.uuid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public String getPwd() {
		return pwd;
	}
	public void setPwd(String pwd) {
		this.pwd = pwd;
	}
	public String toString(){
		return String.valueOf(this.uuid) + ": " + this.name +"; "+ UserTypeEnum.getNameByType(type);
	}
	@Override
	public int hashCode() {
		final int primary = 31;
		int result = 1;
		result = primary*result + ((name==null)? 0 :name.hashCode());
		result = primary*result +uuid;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if(obj == null){
			return false;
		}else if(getClass() != obj.getClass()){
			return false;
		}
		UserModel other = (UserModel)obj;
		if (name == null){
			if(other.getName()!=null){
				return false;
				}
			}else if(!name.equals(other.getName())){
				return false;
			}
		if(uuid != other.getUuid()){
			return false;
		}
		return true;
		}
	
}
