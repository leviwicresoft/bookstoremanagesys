package bookstoreMS.user.vo;

import java.io.Serializable;

public class UuidModel implements Serializable {
	 /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String modelName;
	 private int nextUuid;
	public String getModelName() {
		return modelName;
	}
	public void setModelName(String modelName) {
		this.modelName = modelName;
	}
	public int getNextUuid() {
		return nextUuid;
	}
	public void setNextUuid(int nextUuid) {
		this.nextUuid = nextUuid;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((modelName == null) ? 0 : modelName.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UuidModel other = (UuidModel) obj;
		if (modelName == null) {
			if (other.modelName != null)
				return false;
		} else if (!modelName.equals(other.modelName))
			return false;
		return true;
	}
	 
}
