package bookstoreMS.user.business.factory;

import bookstoreMS.user.business.ebi.UserEbi;
import bookstoreMS.user.business.ebo.UserEbo;

public class UserEbiFactory {
	public static UserEbi getUserEbo(){
		return new UserEbo();
	}
	
}
