package bookstoreMS.user.business.factory;

import bookstoreMS.user.business.ebi.UuidEbi;
import bookstoreMS.user.business.ebo.UuidEbo;
import bookstoreMS.user.dao.factory.UuidDaoFactory;

public class UuidEbiFactory {
public static UuidEbi getUuidEbo(){
	return new UuidEbo();
}
}
