package bookstoreMS.user.business.ebo;

import java.util.ArrayList;

import bookstoreMS.user.business.ebi.UserEbi;
import bookstoreMS.user.dao.dao.UserDao;
import bookstoreMS.user.dao.factory.UserDaoFactory;
import bookstoreMS.user.vo.QueryModel;
import bookstoreMS.user.vo.UserModel;

public class UserEbo implements UserEbi{

	@Override
	public boolean add(UserModel user) {
		UserDao dao = UserDaoFactory.getUserDao();
		return dao.add(user);
	}

	@Override
	public boolean update(UserModel user) {
		UserDao dao = UserDaoFactory.getUserDao();
		return dao.update(user);
	}

	@Override
	public boolean delete(int uuid) {
		UserDao dao = UserDaoFactory.getUserDao();
		return dao.delete(uuid);
	}

	@Override
	public ArrayList<UserModel> getAll() {
		UserDao dao = UserDaoFactory.getUserDao();
		return dao.getAll();
	}

	@Override
	public UserModel getSingle(int uuid) {
		UserDao dao = UserDaoFactory.getUserDao();
		return dao.getSingle(uuid);
	}

	@Override
	public ArrayList getByCondition(QueryModel qm) {
		UserDao dao = UserDaoFactory.getUserDao();
		return dao.getByCondition(qm);
	}

}
