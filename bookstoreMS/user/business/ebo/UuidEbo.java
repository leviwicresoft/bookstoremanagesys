package bookstoreMS.user.business.ebo;

import bookstoreMS.user.business.ebi.UuidEbi;
import bookstoreMS.user.dao.factory.UuidDaoFactory;

public class UuidEbo implements UuidEbi {
	public int getUuid(String modelName){
		return UuidDaoFactory.getUuidDao().getNextUuid(modelName);
	}
}
