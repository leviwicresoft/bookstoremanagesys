package bookstoreMS.user;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import bookstoreMS.user.vo.UserModel;

public class ReadFromFile {
	public static List list = new ArrayList();
 public static List RFFile(String fileLocation){
	 
				
		ObjectInputStream input = null;
		
		try{
		File file = new File(fileLocation);
		
		if (file.exists()){
			input = new ObjectInputStream(new FileInputStream(file));
			list = (List)input.readObject();
						
			input.close();
						
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return list;
 }
 @SuppressWarnings("unchecked")
public static void STFile(List list, String fileLocation){
	 
	 ObjectOutputStream output = null;
	 
		try{
		output = new ObjectOutputStream(new FileOutputStream(fileLocation));
		output.writeObject(list);
		output.close();
		
		}catch(Exception e){
			e.printStackTrace();
		}
		
 }

 }

 
