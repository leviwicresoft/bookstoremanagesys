package bookstoreMS.user.dao;

public enum UserTypeEnum {
	ADMIN(1,"SuperAdmin"),
	IN(2,"InAdmin"),
	OUT(3,"OutAdmin"),
	STOCK(4,"StockAdmin");
	
	private final int type;
	private final String name;
	private UserTypeEnum(int type, String name){
		this.type = type;
		this.name= name;
	}
	public int getType(){
		return this.type;
	}
	public String getName(){
		return this.name;
	}
	public String toString(){
		return this.name();
	}
	public static String getNameByType(int type){
		for (UserTypeEnum usertype : UserTypeEnum.values()){
			if (usertype.getType()==type){
				return usertype.getName();
			}
		}
		throw new IllegalArgumentException();
	}
	public static int getTypeByName(String name){
		for (UserTypeEnum usertype : UserTypeEnum.values()){
			if (usertype.getName().equals(name)){
				return usertype.getType();
			}
		}
		throw new IllegalArgumentException();
	}
}
