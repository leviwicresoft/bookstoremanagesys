package bookstoreMS.user.dao.factory;

import bookstoreMS.user.dao.dao.UuidDao;
import bookstoreMS.user.dao.impl.UuidDaoImpl;

public class UuidDaoFactory {
	public static UuidDao getUuidDao(){
		return new UuidDaoImpl();
	}
}
