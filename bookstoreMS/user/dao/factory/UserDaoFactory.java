package bookstoreMS.user.dao.factory;

import bookstoreMS.user.dao.dao.UserDao;
import bookstoreMS.user.dao.impl.UserDaoSerImpl;

public class UserDaoFactory {
   public static UserDao getUserDao(){
	   return new UserDaoSerImpl();
   }
}
