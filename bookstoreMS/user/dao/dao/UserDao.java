package bookstoreMS.user.dao.dao;

import java.util.ArrayList;

import bookstoreMS.user.vo.QueryModel;
import bookstoreMS.user.vo.UserModel;

public interface UserDao {
	public boolean add(UserModel user) ;
	public boolean update(UserModel user);
	public boolean delete(int uuid);
	public ArrayList<UserModel> getAll();
	public UserModel getSingle(int uuid);
	public ArrayList getByCondition(QueryModel qm);
}
