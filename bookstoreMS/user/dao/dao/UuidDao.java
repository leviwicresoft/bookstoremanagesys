package bookstoreMS.user.dao.dao;

public interface UuidDao {
	public int getNextUuid(String modelName);
}
