package bookstoreMS.user.dao.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import bookstoreMS.user.ReadFromFile;
import bookstoreMS.user.dao.dao.UserDao;
import bookstoreMS.user.vo.QueryModel;
import bookstoreMS.user.vo.UserModel;
import bookstoreMS.user.vo.UuidModel;

public class UserDaoSerImpl implements UserDao{
	String fileLocation = "c://code/user.txt";
	
	@SuppressWarnings("unchecked")
	@Override
	public boolean add(UserModel user){
		List list = new ArrayList();
		list = ReadFromFile.RFFile(fileLocation);
		for (Object obj : list){
			UserModel temp = (UserModel)obj;
			if(temp.getUuid() == user.getUuid()){
				return false;
			}
		}
		list.add(user);
		ReadFromFile.STFile(list,fileLocation);
		
		return true;
	}
	@SuppressWarnings("unchecked")
	@Override
	public boolean update(UserModel user) {
		List list = ReadFromFile.RFFile(fileLocation);
		for (int index = 0; index < list.size(); index ++){
			UserModel temp = (UserModel)list.get(index);
			if(temp.getUuid() == user.getUuid()){
				list.set(index, user);
				ReadFromFile.STFile(list, fileLocation);
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean delete(int uuid) {
		List list = ReadFromFile.RFFile(fileLocation);
	
		for(int index = 0; index<list.size(); index ++){
			UserModel user = (UserModel)list.get(index);
		
		
			if (user.getUuid() == uuid){
				list.remove(index);
				
				ReadFromFile.STFile(list, fileLocation);
				return true;
			}
			
			
		}
		return false;
	}

	@SuppressWarnings("unchecked")
	@Override
	public ArrayList<UserModel> getAll() {
		List list = ReadFromFile.RFFile(fileLocation);
		return (ArrayList<UserModel>) list;
	}

	@Override
	public UserModel getSingle(int uuid) {
		List list = ReadFromFile.RFFile(fileLocation);
		for(Object obj : list){
			UserModel user = (UserModel)obj;
			if (user.getUuid() == uuid){
				return user;
			}
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public ArrayList getByCondition(QueryModel qm) {
	       ArrayList qmlist = new ArrayList();
	       ArrayList<UserModel> userlist = (ArrayList<UserModel>)ReadFromFile.RFFile(fileLocation);
	       for(UserModel user: userlist){
	    	  if(qm.getUuid()>0 && qm.getUuid()!=user.getUuid()){
	    		  continue;
	    	  }else if(qm.getName()!=""&& user.getName().indexOf(qm.getName())<0){
	    		  continue;
	    	  }else if (qm.getType()>0 && qm.getType() != user.getType()){
	    			  continue;
	    		  }
	    	   qmlist.add(user);
	         }
		   return qmlist;
	}
	public static void main(String agrs[]){
		UserModel user = new UserModel();
		user.setUuid(111);
		user.setName("Eric");
		user.setType(2);
		user.setPwd("1445");
		UserDao userebi = new UserDaoSerImpl();
		userebi.update(user);
		System.out.println(userebi.getAll());
		
	}

}
