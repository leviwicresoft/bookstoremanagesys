package bookstoreMS.user.dao.impl;

import java.util.ArrayList;
import java.util.List;

import bookstoreMS.user.ReadFromFile;
import bookstoreMS.user.dao.UserTypeEnum;
import bookstoreMS.user.dao.dao.UuidDao;
import bookstoreMS.user.vo.UuidModel;

public class UuidDaoImpl implements UuidDao{
	private String idFile = "c://code/Uuid.txt";
	public int getNextUuid(String modelName){
		List<UuidModel> list = ReadFromFile.RFFile(idFile);
		int result=0;
		
		switch(modelName){
		
		case "SuperAdmin":{
			result = 100000;
			break;
		}
		case "InAdmin":{
			result = 200000;
			break;
		}
		case "OutAdmin":{
			result =  300000;
			break;
		}
		case "StockAdmin":{
			result = 400000;
			break;
		}
		}
		for(UuidModel model : list){
			if(model.getModelName().equals(modelName)){
				result = model.getNextUuid();
				model.setNextUuid(result +1);
				ReadFromFile.STFile(list,idFile);
				return result;
			}
		}
		UuidModel newModel = new UuidModel();
		newModel.setModelName(modelName);
		newModel.setNextUuid(result+1);
		list.add(newModel);
		ReadFromFile.STFile(list,idFile);
		
		return result;
	}
	public static void main(String args[]){
		UuidDaoImpl model = new UuidDaoImpl();
		System.out.println(model.getNextUuid("SuperAdmin"));
	}
	
}
