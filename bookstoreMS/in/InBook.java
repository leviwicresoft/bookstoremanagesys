package bookstoreMS.in;

import java.io.Serializable;
import java.sql.Time;

import bookstoreMS.BookVO.Book;
import bookstoreMS.BookVO.uBookId;
public class InBook extends Book implements Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String inDate;
    private int inBatchId;
    private int inVolume;
    
    public void setInVolume(int volume){
    	this.inVolume = volume;
    }
    public int getInVolume(){
    	return this.inVolume;
    }
	public String getInDate() {
		return inDate;
	}
	public void setInDate(String time){
		this.inDate = time;
	}
	public void setInBatchId( int id){
		this.inBatchId = id;
	}
	public int getInBatchId() {
		return this.inBatchId;	
		}
	public String toString(){
		return inBatchId + ": " + ubid + ": " + bookName + ": " +  inVolume + ": " + inDate;
	}
}
