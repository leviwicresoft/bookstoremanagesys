package bookstoreMS.in;

import java.util.Date;
import java.util.List;

import bookstoreMS.BookVO.Book;
import bookstoreMS.BookVO.uBookId;
import bookstoreMS.in.dao.factory.InBookFactory;

public class Test{
	public static void main(String args[]){
		InBook newbook = new InBook();
		newbook.setBookName("Effective programming");
		newbook.setCost(Integer.parseInt("20"));
		newbook.setInBatchId(uBookId.getUbid("new Batch"));
		newbook.setInDate(new Date().toString());
		newbook.setInVolume(Integer.parseInt("30"));
		newbook.setUbid(uBookId.getUbid("new Book"));
		InBookFactory.getInBookDao().add(newbook);
		List<InBook> booklist = InBookFactory.getInBookDao().getAll();
		for (InBook b : booklist){
			System.out.println(b);
		}
	}
}