package bookstoreMS.in.panel;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;

import bookstoreMS.BookVO.Book;
import bookstoreMS.BookVO.uBookId;
import bookstoreMS.in.InBook;
import bookstoreMS.in.business.factory.InbookEbiFactory;
import bookstoreMS.in.dao.factory.InBookFactory;

public class AddPanel extends JFrame {
	
	public ArrayList getAllName(){
		ArrayList<InBook> allBook = InbookEbiFactory.getInbookEbi().getAll();
		ArrayList<String> allbookName = new ArrayList<String>();
		for(Book b: allBook){
			if(!allbookName.contains(b.getBookName())){
				allbookName.add(b.getBookName());
			}
		}
			return allbookName;
		}
	public AddPanel(JFrame frame){
		frame.setSize(600,700);
		frame.setLocation(100, 100);
		frame.setLayout(null);
		JPanel panel = new JPanel();
		panel.setLayout(null);
		panel.setSize(600,700);
		
		JPanel panelbookdetail = new JPanel(); // a panel to seperate the inbook details
		panelbookdetail.setLocation(120,150);
		panelbookdetail.setLayout(null);
		panelbookdetail.setSize(350, 300);
		
		panelbookdetail.setBorder(new TitledBorder(new LineBorder(Color.GRAY,2),"Book Details",TitledBorder.LEFT, TitledBorder.TOP, new Font("Serif",Font.ITALIC,15)));
		
		//panelbookdetail.setBorder(new TitledBorder(new LineBorder(Color.GRAY,2),"Text",TitledBorder.LEFT,TitledBorder.TOP, new Font("Serif",Font.ITALIC,20)));
		
		
		Font titlefont = new Font("Serif", Font.BOLD,20);
		JLabel titlelabel = new JLabel("Add Book");
		titlelabel.setFont(titlefont);
		titlelabel.setBounds(250,30,200,40);
		JLabel batchIdLabel = new JLabel("Batch ID");
		batchIdLabel.setBounds(150,100,100,20);
		JTextField batchid = new JTextField();
		batchid.setBounds(200, 100, 70, 20);
		JButton getidbutton = new JButton("Get BatchID");
		getidbutton.setBounds(300, 100,100, 20);
		getidbutton.addActionListener( new ActionListener(){
			public void actionPerformed(ActionEvent e){
				batchid.setText(uBookId.getUbid("New Batch")+"");
				getidbutton.setEnabled(false);
				}
		});
		JLabel BookIdLabel = new JLabel("Book ID");
		BookIdLabel.setBounds(30,50,100,20);
		JTextField bookid = new JTextField();
		bookid.setBounds(90, 50, 70, 20);
		JButton getbookidbutton = new JButton("Get BookID");
		getbookidbutton.setBounds(170,50,100,20);
		getbookidbutton.addActionListener( e -> bookid.setText(uBookId.getUbid("New Book")+""));
		
		JLabel bookNameLabel = new JLabel("Book Name");
		bookNameLabel.setBounds(20,80,120,20);
		JTextField bookName = new JTextField();
		bookName.setBounds(90, 80, 200, 20);
		
		// get all the books' names
		@SuppressWarnings("unchecked")
		
		// set the prompted matched book  name
		Vector<String> cachedName = new Vector<String>();
		JList jlist = new JList();
		JScrollPane scroll = new JScrollPane(jlist);
		scroll.setBounds(90, 100, 200, 100);
		scroll.setVisible(false);
		
		jlist.addMouseListener(new MouseListener(){

			@Override
			public void mouseClicked(MouseEvent e) {
				
				bookName.setText((String)jlist.getSelectedValue());
				scroll.setVisible(false);
			}

			@Override
			public void mouseEntered(MouseEvent arg0) {}
			public void mouseExited(MouseEvent arg0) {}
			public void mousePressed(MouseEvent arg0) {}
			public void mouseReleased(MouseEvent arg0) {			}
			
		});
		
		DocumentListener bookNameListener = new DocumentListener(){
			
			@Override
			public void changedUpdate(DocumentEvent e) {
				inputchanged( e);
				}
		@Override
			public void insertUpdate(DocumentEvent e) {
				inputchanged( e);
			}
			@Override
			public void removeUpdate(DocumentEvent e) {
				inputchanged( e);
				}
			@SuppressWarnings("unchecked")
			public void inputchanged(DocumentEvent e){
				Document document = e.getDocument();
				String text = null;
				try {
					text = document.getText(0, document.getLength());
				} catch (BadLocationException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				cachedName.clear();
				ArrayList<String> allName = getAllName();
				if(text!=null && !text.trim().equals("")){
						for(String string : allName){
							if(string.toLowerCase().contains(text.toLowerCase())){
								cachedName.add(string);
							}else{
								scroll.setVisible(false);
							}
					}	
				}
				if(!cachedName.isEmpty()){
				jlist.setListData(cachedName);
				jlist.validate();
				scroll.setVisible(true);
				panelbookdetail.add(scroll);
				panelbookdetail.setComponentZOrder(scroll,1); // set Zorder as the lowest to paint last
				}
			}
		};
		bookName.getDocument().addDocumentListener(bookNameListener);
		
				
		JLabel quantityLabel = new JLabel("Quantity");
		quantityLabel.setBounds(30,110,70,20);
		JTextField quantity = new JTextField();
		quantity.setBounds(90,110,100,20);
		
		JLabel unitPriceLabel = new JLabel("Unit inPrice");
		unitPriceLabel.setBounds(15,140,70,20);
		JTextField inPrice = new JTextField();
		inPrice.setBounds(90,140,100,20);
		
		JList jbooklist = new JList();
		JScrollPane bookdetailpane = new JScrollPane(jbooklist);
		bookdetailpane.setBounds(30,180,300,100);
		Vector<InBook> inbookdetail = new Vector<InBook>();
		
		JButton addunit = new JButton("ADD");
		addunit.setBounds(220, 115, 70, 40);
		
		
	      addunit.addActionListener(new ActionListener(){
			@SuppressWarnings("unchecked")
			public void actionPerformed(ActionEvent e){
				InBook newbook = new InBook();
				newbook.setBookName(bookName.getText());
				newbook.setCost(Integer.parseInt(inPrice.getText()));
				
				newbook.setInDate(new Date().toString());
				newbook.setInVolume(Integer.parseInt(quantity.getText()));
				
				if(batchid.getText().equals("") ||  bookid.getText().equals("")){
					JOptionPane.showMessageDialog(null, "Please set BatchId or BookId");
				}
				newbook.setUbid(Integer.parseInt(bookid.getText()));
				bookid.setText("");
				newbook.setInBatchId(Integer.parseInt(batchid.getText()));
				inbookdetail.add(newbook);
			    jbooklist.setListData(inbookdetail);
			    jbooklist.validate();
				
			}
		});
	    JButton confirm = new JButton("Confirm Add");
		confirm.setBounds(220, 450, 150, 40);
	    confirm.addActionListener(new ActionListener(){
	    	public void actionPerformed(ActionEvent e){
	    		for(InBook b:inbookdetail ){
	    			InBookFactory.getInBookDao().add(b);
	    		}
	    		inbookdetail.clear();
	    		JOptionPane.showMessageDialog(null, "Saved Successfully!");
	    		jbooklist.setListData(inbookdetail);
	    		jbooklist.validate();
	    	}
	    	
	    });
	    
		panel.add(confirm);
		panelbookdetail.add(bookdetailpane);
		panelbookdetail.add(addunit);
		panelbookdetail.add(unitPriceLabel);
		panelbookdetail.add(inPrice);
		panelbookdetail.add(quantityLabel);
		panelbookdetail.add(quantity);
		//panelbookdetail.setComponentZOrder(quantity,2);
		panelbookdetail.add(bookName);
		panelbookdetail.add(bookNameLabel);
		//panel.add();
		panelbookdetail.add(BookIdLabel);
		panelbookdetail.add(bookid);
		bookid.setEditable(false);
		panelbookdetail.add(getbookidbutton);
		panel.add(getidbutton);
		panel.add(batchid);
		batchid.setEditable(false);
		panel.add(panelbookdetail);
		panel.add(batchIdLabel);
		panel.add(titlelabel);
		frame.add(panel);
		frame.setVisible(true);
	}
	
	public static void main(String args[]){
		JFrame frame = new	JFrame();
		new AddPanel(frame);
	
	}
	
}
