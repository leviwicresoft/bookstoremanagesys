package bookstoreMS.in.panel;

import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class InbookList extends JFrame{
	
	public InbookList(JFrame frame){
		frame.setLocation(100,100);
		frame.setSize(600,700);
		frame.setLayout(null);
		JPanel panel = new JPanel();
		panel.setSize(600,800);
		
		panel.setLayout(null);
		Font font = new Font("Serif",Font.BOLD,30);
		Font buttonfont = new Font("Serif",Font.BOLD,20);
		JLabel label = new JLabel();
		label.setFont(font);
		label.setText("InBook Panel");
		label.setBounds(220,50,200,40);
		
		JButton addbutton = new JButton("Add Book");
		addbutton.setFont(buttonfont);
		addbutton.setBounds(120,150,150,150);
		
		JButton deletebutton = new JButton("Delete Book");
		deletebutton.setFont(buttonfont);
		deletebutton.setBounds(330,150,150,150);
		
		JButton updatebutton = new JButton("Update Book");
		updatebutton.setFont(buttonfont);
		updatebutton.setBounds(120,330,150,150);
		
		JButton searchbutton = new JButton("Search Book");
		searchbutton.setFont(buttonfont);
		searchbutton.setBounds(330,330,150,150);
		
		panel.add(updatebutton);
		panel.add(searchbutton);
		panel.add(deletebutton);
		panel.add(label);
		panel.add(addbutton);
		frame.add(panel);
		frame.setVisible(true);
		
		
	}
	
	
}
