package bookstoreMS.in.dao.dao;

import java.util.ArrayList;

import bookstoreMS.in.InBook;

public interface InBookDao {
	public boolean add(InBook inbook);
	public boolean delete(int ubid);
	public boolean update(InBook inbook);
	public ArrayList getAll();
	public InBook getSingle(int ubid);
	public ArrayList getByCondition();
	
}
