package bookstoreMS.in.dao.factory;

import java.util.Date;

import bookstoreMS.BookVO.ReadTFFile;
import bookstoreMS.BookVO.uBookId;
import bookstoreMS.in.InBook;
import bookstoreMS.in.dao.dao.InBookDao;
import bookstoreMS.in.dao.impl.InBookImpl;

public class InBookFactory {
  public static InBookDao getInBookDao(){
	  return new InBookImpl();
  }
  
}

