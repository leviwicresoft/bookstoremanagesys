package bookstoreMS.in.dao.impl;

import java.util.Date;
import java.util.ArrayList;
import java.util.List;

import bookstoreMS.BookVO.Book;
import bookstoreMS.BookVO.ReadTFFile;
import bookstoreMS.BookVO.uBookId;
import bookstoreMS.in.InBook;
import bookstoreMS.in.dao.dao.InBookDao;
import bookstoreMS.in.dao.factory.InBookFactory;

public class InBookImpl implements InBookDao {
	String fileLocation = "c://code/inbook.txt";
	@SuppressWarnings("unchecked")
	@Override
	public boolean add(InBook inbook) {
		List<InBook> inbooklist=  new ArrayList<InBook>();
		inbooklist = (ArrayList<InBook>)ReadTFFile.readFromFile(fileLocation, inbooklist);
		for(InBook each : inbooklist){
			if(each.getUbid() == inbook.getUbid()){
				return false;
			}
		}
		inbooklist.add(inbook);
		ReadTFFile.readToFile(fileLocation,inbooklist);
		return true;
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean delete(int ubid) {
		List<InBook> inbooklist=  new ArrayList<InBook>();
		inbooklist = (ArrayList<InBook>)ReadTFFile.readFromFile(fileLocation, inbooklist);
		for(InBook each : inbooklist){
			if (each.getUbid()==ubid){
				inbooklist.remove(each);
				ReadTFFile.readToFile(fileLocation, inbooklist);
				return true;
			}
		}
		return false;
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean update(InBook inbook) {
		List<InBook> inbooklist=  new ArrayList<InBook>();
		inbooklist = (ArrayList<InBook>)ReadTFFile.readFromFile(fileLocation, inbooklist);
		for(int i = 0; i<inbooklist.size();i++){
			if(inbooklist.get(i).getUbid()==inbook.getUbid()){
				inbooklist.set(i, inbook);
				return true;
			}
		}
		return false;
	}

	@SuppressWarnings("unchecked")
	@Override
	public ArrayList getAll() {
		ArrayList<InBook> inbooklist=  new ArrayList<InBook>();
		inbooklist = (ArrayList<InBook>)ReadTFFile.readFromFile(fileLocation, inbooklist);
		return inbooklist;
	}

	@SuppressWarnings("unchecked")
	@Override
	public InBook getSingle(int ubid) {
		List<InBook> inbooklist=  new ArrayList<InBook>();
		inbooklist = (ArrayList<InBook>)ReadTFFile.readFromFile(fileLocation, inbooklist);
		for(int i = 0; i<inbooklist.size();i++){
			if(inbooklist.get(i).getUbid()==ubid){
				return inbooklist.get(i);
			}
		}
		return null;
	}

	@Override
	public ArrayList getByCondition() {
		// TODO Auto-generated method stub
		return null;
	}
	
	}

