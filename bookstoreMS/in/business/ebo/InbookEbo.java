package bookstoreMS.in.business.ebo;

import java.util.ArrayList;

import bookstoreMS.in.InBook;
import bookstoreMS.in.business.ebi.InbookEbi;
import bookstoreMS.in.dao.dao.InBookDao;
import bookstoreMS.in.dao.factory.InBookFactory;

public class InbookEbo implements InbookEbi{
	public boolean add(InBook inbook){
		InBookDao inbookdao = InBookFactory.getInBookDao();
		return inbookdao.add(inbook);
	}
	public boolean delete(int ubid){
		InBookDao inbookdao = InBookFactory.getInBookDao();
		return inbookdao.delete(ubid);
	}
	public boolean update(InBook inbook){
		InBookDao inbookdao = InBookFactory.getInBookDao();
		return inbookdao.update(inbook);
	}
	public ArrayList getAll(){
		InBookDao inbookdao = InBookFactory.getInBookDao();
		return inbookdao.getAll();
	}
	public InBook getSingle(int ubid){
		InBookDao inbookdao = InBookFactory.getInBookDao();
		return inbookdao.getSingle(ubid);
	}
	public ArrayList getByCondition(){
		return null;
	}
}
