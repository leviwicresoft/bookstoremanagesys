package bookstoreMS.in.business.factory;

import bookstoreMS.in.business.ebi.InbookEbi;
import bookstoreMS.in.business.ebo.InbookEbo;

public class InbookEbiFactory {
	public static InbookEbi getInbookEbi(){
		return new InbookEbo();
	}
}
