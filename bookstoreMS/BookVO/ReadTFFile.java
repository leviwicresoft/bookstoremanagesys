package bookstoreMS.BookVO;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;

public class ReadTFFile {
	public static Object readFromFile(String fileLocation, Object obj){
		File file = new File(fileLocation);
		Object reObj = null;
		if(!file.exists()){
			try{
			file.createNewFile();
			ReadTFFile.readToFile(fileLocation,obj.getClass().newInstance());
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		try{
		ObjectInputStream input = new ObjectInputStream(new FileInputStream(file));
		//System.out.println("Here------------Here");
		reObj = input.readObject();
		//System.out.println("After-------After!");
		input.close();
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return reObj;
    }
	
	public static void readToFile(String fileLocation, Object obj){
		File file = new File(fileLocation);
		
		if(!file.exists()){
			try{
			file.createNewFile();
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		try{
		ObjectOutputStream output = new ObjectOutputStream(new FileOutputStream(fileLocation));
		output.writeObject(obj);
		output.close();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
