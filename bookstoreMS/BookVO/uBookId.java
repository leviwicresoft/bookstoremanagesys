package bookstoreMS.BookVO;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class uBookId {
	static Map<String, Integer> bidMap = new HashMap<String, Integer>();
	

	static String fileLocation = "c://Code/ubid.txt";
	
	static int result = 1;
	@SuppressWarnings({ "resource", "unchecked" })
	public static int getUbid(String book){
		bidMap = (Map<String, Integer>)ReadTFFile.readFromFile(fileLocation, bidMap);
		
		if(bidMap!=null && bidMap.containsKey(book)){
			result = bidMap.get(book);
			
			bidMap.put(book, result+1);
			ReadTFFile.readToFile(fileLocation, bidMap);
			return result;
			
		}else{
			bidMap.put(book, result+1);
			ReadTFFile.readToFile(fileLocation, bidMap);
			return result;
		}
}
	public static void main(String args[]){
		System.out.println(uBookId.getUbid("newBatch"));
	}
}	

