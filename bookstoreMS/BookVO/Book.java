package bookstoreMS.BookVO;

import java.io.Serializable;

public class Book implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected int ubid;
	protected String bookName;
	protected double cost;
	public int getUbid() {
		return this.ubid;
	}
	public void setUbid(int ubid) {
		this.ubid = ubid;
	}
	public String getBookName() {
		return bookName;
	}
	public void setBookName(String bookName) {
		this.bookName = bookName;
	}
	public double getCost() {
		return cost;
	}
	public void setCost(double cost) {
		this.cost = cost;
	}
	public int hashCode(){
		int result=1;
		final int primary = 32;
		result = primary*result + ((this.bookName ==null)? 0:bookName.hashCode());
		result = result+this.ubid;
		return result;
	}
	public boolean equals(Object obj){
		if(obj == null){
			return false;
		}else if(obj.getClass() !=this.getClass()){
			return false;
		}
		Book other = (Book)obj;
		if(this.bookName ==null ){
			return false;
		}else if(!this.bookName.equals(other.getBookName())){
			return false;
		}
		if(this.ubid != other.getUbid()){
			return false;
		}
			return true;
	}
	
}
